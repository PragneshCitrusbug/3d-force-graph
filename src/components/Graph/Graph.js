import React from "react";

import ForceGraph3D from "3d-force-graph";
import Data from "../../papers.json";

function Graph() {
  // Random tree
  const N = 300;
  const gData = {
    nodes: Data.nodes, //[...Array(N).keys()].map((i) => ({ id: i })),
    links: Data.links //[...Array(N).keys()]
      .filter((id) => id)
      .map((id) => ({
        source: id,
        target: Math.round(Math.random() * (id - 1)),
      })),
  };

  const Graph = ForceGraph3D()(document.getElementById("3d-graph")).graphData(
    gData
  );

  return <div id="3d-graph"></div>;
}

export default Graph;
