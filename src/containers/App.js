import React, { useState, Component } from "react";
import "./App.css";
import ForceGraph3D from "react-force-graph-3d";
import SpriteText from "three-spritetext";
import Data from "../papers.json";

class App extends Component {
  constructor(props) {
    // Required step: always call the parent class' constructor
    super(props);

    // Set the state directly. Use props if necessary.
    this.state = {
      nodes: this.genRandomTree(26, false),
      backgroundColor: "black",
      enableNodeDrag: false,
      showNavInfo: true,
      nodeLabel: "id",
      nodeAutoColorBy: "group",
      enableNavigationControls: false,
      distance: 600,
      isRotationActive: true,
      activeNode: false,
      angle: 0,
    };
    this.fgRef = React.createRef();
  }

  componentDidMount() {
    this.setState({
      activeNode: this.state.nodes.nodes[0],
    });

    const distance = this.state.distance;

    setInterval(() => {
      if (this.state.isRotationActive) {
        this.fgRef.current.cameraPosition(
          {
            x: distance * Math.sin(this.state.angle),
            z: distance * Math.cos(this.state.angle),
          },
          this.state.activeNode
        );

        this.setState({
          angle: (this.state.angle += Math.PI / 300),
        });
      }
    }, 10);
  }

  genRandomTree = (N = 10, reverse = false) => {
    console.log("Nodes", Data.nodes.map((i,key) => ({ id: i.id , name: i.id, value: key,   })))
    console.log("links", Data.links.map((i,key) => ({ source: i.source , target: i.target, value: key,   })))
    return {
      nodes: Data.nodes.map((i,key) => ({ id: i.id , name: i.id, value: key,   })), //[...Array(N).keys()].map((i) => ({ id: i })),
      links: Data.links.map((i,key) => ({ source: i.source , target: i.target, value: i.value,   })) //[...Array(N).keys()]
        // .filter((id) => id)
        // .map((id) => {
        //   return {
        //     [reverse ? "target" : "source"]: id.value,
        //     [reverse ? "source" : "target"]: Math.round(
        //       Math.random() * (id.value - 1)
        //     ),
        //   };
        // }),
    };
  };

  handleClick = (node, event) => {
    console.log(node);

    this.setState({
      activeNode: node,
    });

    const distance = this.state.distance;
    const distRatio = 1 + distance / Math.hypot(node.x, node.y, node.z);

    this.fgRef.current.cameraPosition(
      { x: node.x, y: node.y }, // new position
      node, // lookAt ({ x, y, z })
      3000 // ms transition duration
    );

    setTimeout(() => {
      this.setState({
        isRotationActive: true,
      });
    }, 3000);
  };

  animationHandler(event) {
    this.setState({
      isRotationActive: !this.state.isRotationActive,
    });
  }

  render() {
    return (
      <div className="App">
        <button onClick={(e) => this.animationHandler(e)}>
          {" "}
          {this.state.isRotationActive ? "Pause" : "Resume"} Rotation{" "}
        </button>

        <ForceGraph3D
          graphData={this.state.nodes}
          enableNodeDrag={this.state.enableNodeDrag}
          backgroundColor={this.state.backgroundColor}
          showNavInfo={this.state.showNavInfo}
          nodeLabel={this.state.nodeLabel}
          nodeAutoColorBy={this.state.nodeAutoColorBy}
          onNodeClick={this.handleClick}
          enableNavigationControls={this.state.enableNavigationControls}
          ref={this.fgRef}
          linkThreeObjectExtend={true}
          nodeThreeObjectExtend={true}
          // linkThreeObject={link => {
          //   // extend link with text sprite
          //   const sprite = new SpriteText(`${link.source} > ${link.target}`);
          //   sprite.color = 'lightgrey';
          //   sprite.textHeight = 1.5;
          //   return sprite;
          // }}
          // linkPositionUpdate={(sprite, { start, end }) => {
          //   const middlePos = Object.assign(...['x', 'y', 'z'].map(c => ({
          //     [c]: start[c] + (end[c] - start[c]) / 2 // calc middle point
          //   })));

          //   // Position sprite
          //   Object.assign(sprite.position, middlePos);
          // }}
          nodeThreeObject={(node) => {
            const sprite = new SpriteText(node.id);
            sprite.color = node.color;
            sprite.textHeight = 8;
            sprite.position.set(0, -10, 0);
            return sprite;
          }}
          // cooldownTicks={100}
          // onEngineStop={() => this.fgRef.current.zoomToFit(400)}
        />
      </div>
    );
  }
}

// const App2  = props => {

//   let nodeNumber = 5;

//   const genRandomTree = (N = 10, reverse = false) => {

//     return {
//       nodes: [...Array(N).keys()].map(i => ({ id: i })),
//         links: [...Array(N).keys()]
//       .filter(id => id)
//       .map(id => ({
//         [reverse ? 'target' : 'source']: id,
//         [reverse ? 'source' : 'target']: Math.round(Math.random() * (id-1))
//       }))
//     };
//   }

//   const data = genRandomTree();

//   const [ nodeState, setNodeState ] = useState({
//     data: genRandomTree( nodeNumber, true),

//   });

//   const { useRef, useCallback } = React;

//   const fgRef = useRef();

//   const distance = 100;

//   const handleClick = useCallback(node => {

//     // Aim at node from outside it
//     console.log('handleClick')

//     const distRatio = 1 + distance/Math.hypot(node.x, node.y, node.z);

//     fgRef.current.cameraPosition(
//       { x: node.x * distRatio, y: node.y * distRatio, z: node.z * distRatio }, // new position
//       node, // lookAt ({ x, y, z })
//       3000  // ms transition duration
//     );
//   }, [fgRef,distance]);

//   let angle = 0;

//   const rotate = useCallback(node => {

//     fgRef.current.cameraPosition({
//       x: distance * Math.sin(angle),
//       z: distance * Math.cos(angle)
//     });

//     angle += Math.PI / 300;

//   }, [fgRef,distance,angle])

//     setInterval(() => {
//       // rotate()
//     }, 10);

//   const addNodeHandler = () => {
//     nodeNumber = nodeNumber+1;
//     setNodeState({
//       data :  genRandomTree( nodeNumber, true),
//     })

//   }

//     return (
//       <div className="App">
//         <button onClick={addNodeHandler} >Add none</button>
//         <ForceGraph3D
//             graphData={nodeState.data}
//             ref={fgRef}
//             enableNodeDrag={false}
//             // width={ 600}
//             // height= {700}
//             backgroundColor = {'black'}
//             showNavInfo= {false}
//             nodeLabel="id"
//             nodeAutoColorBy="group"
//             onNodeClick={handleClick}
//             enableNavigationControls={true}
//             cameraPosition= {{ z: distance }}
//           />
//       </div>
//     );

// }

// function App() {

//   function genRandomTree(N = 10, reverse = false) {
//     return {
//       nodes: [...Array(N).keys()].map(i => ({ id: i })),
//         links: [...Array(N).keys()]
//       .filter(id => id)
//       .map(id => ({
//         [reverse ? 'target' : 'source']: id,
//         [reverse ? 'source' : 'target']: Math.round(Math.random() * (id-1))
//       }))
//     };
//   }

//   const data = genRandomTree();

//   console.log(data)

//   const { useRef, useCallback } = React;

//   const fgRef = useRef();

//   const handleClick = useCallback(node => {

//     // Aim at node from outside it
//     console.log('handleClick')
//     const distance = 100;
//     const distRatio = 1 + distance/Math.hypot(node.x, node.y, node.z);

//     fgRef.current.cameraPosition(
//       { x: node.x * distRatio, y: node.y * distRatio, z: node.z * distRatio }, // new position
//       node, // lookAt ({ x, y, z })
//       3000  // ms transition duration
//     );
//   }, [fgRef]);

//   return (
//     <div className="App">
//        <ForceGraph3D
//           graphData={data}
//           ref={fgRef}
//           // width={ 600}
//           // height= {700}
//           backgroundColor = {'black'}
//           showNavInfo= {false}
//           nodeLabel="id"
//           nodeAutoColorBy="group"
//           onNodeClick={handleClick}
//         />
//     </div>
//   );
// }

export default App;
